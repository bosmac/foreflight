import React from "react";
import { MainComponent, NavComponent } from "../../components";
export class MainPage extends React.Component {
  render() {
    return (
      <div>
        <NavComponent></NavComponent>
        <MainComponent></MainComponent>
      </div>
    );
  }
}
