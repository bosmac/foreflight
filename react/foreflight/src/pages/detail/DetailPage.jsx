import React from "react";
import { DetailComponent, NavComponent } from "../../components";

export class DetailPage extends React.Component {
  render() {
    return (
      <div>
        <NavComponent></NavComponent>
        <DetailComponent airportId={this.props.match.params.id}></DetailComponent>
      </div>
    );  }
}
