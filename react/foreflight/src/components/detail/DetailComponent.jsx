import React from "react";

export class DetailComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { apiData: null };
  }

  componentDidMount() {
    fetch(
      "http://localhost:8080/foreflight/v1/airport-weather-data/" +
        this.props.airportId
    )
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        this.setState({ apiData: data });
      });
  }

  render() {
    return (
      <div className="mx-5">
        <ul className="nav nav-tabs" role="tablist">
          <li className="nav-item">
            <a
              className="nav-link active"
              href="#profile"
              role="tab"
              data-toggle="tab"
            >
              Airport
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#buzz" role="tab" data-toggle="tab">
              Weather
            </a>
          </li>
        </ul>
        <div className="tab-content">
          <div role="tabpanel" className="tab-pane fade in active" id="profile">
            {this.state.apiData && this.state.apiData.airtportData && (
              <div>
                <h5>Airport</h5>
                <p>
                  icao: {JSON.stringify(this.state.apiData.airtportData.icao)}
                </p>
                <p>
                  name: {JSON.stringify(this.state.apiData.airtportData.name)}
                </p>
                <p>
                  state: {JSON.stringify(this.state.apiData.airtportData.state)}
                </p>
                <p>
                  country:
                  {JSON.stringify(this.state.apiData.airtportData.country)}
                </p>
                <p>
                  latitude:
                  {JSON.stringify(this.state.apiData.airtportData.latitude)}
                </p>
                <p>
                  longitude:
                  {JSON.stringify(this.state.apiData.airtportData.longitude)}
                </p>
                <p>
                  runways:
                  {JSON.stringify(this.state.apiData.airtportData.runways)}
                </p>
              </div>
            )}
          </div>
          <div role="tabpanel" className="tab-pane fade" id="buzz">
            <div>
              {this.state.apiData &&
                this.state.apiData.weatherData &&
                this.state.apiData.weatherData.conditions && (
                  <div>
                    <h5>Conditions</h5>
                    <p>
                      tempC:
                      {JSON.stringify(
                        this.state.apiData.weatherData.conditions.tempC
                      )}
                    </p>
                    <p>
                      relativeHumidity:
                      {JSON.stringify(
                        this.state.apiData.weatherData.conditions
                          .relativeHumidity
                      )}
                    </p>
                    <p>
                      visibility:
                      {JSON.stringify(
                        this.state.apiData.weatherData.conditions.visibility
                      )}
                    </p>
                    <p>
                      wind:
                      {JSON.stringify(
                        this.state.apiData.weatherData.conditions.wind
                      )}
                    </p>
                    <p>
                      period:
                      {JSON.stringify(
                        this.state.apiData.weatherData.conditions.period
                      )}
                    </p>
                    <hr></hr>
                    <br></br>
                  </div>
                )}
            </div>
            <div>
              {this.state.apiData &&
                this.state.apiData.weatherData &&
                this.state.apiData.weatherData.forecast &&
                this.state.apiData.weatherData.forecast.conditions && (
                  <div>
                    <h5>Forecast</h5>
                    {this.state.apiData.weatherData.forecast.conditions.map(
                      (cond) => {
                        return (
                          <div>
                            <p>
                              tempC:
                              {JSON.stringify(cond.tempC)}
                            </p>
                            <p>
                              relativeHumidity:
                              {JSON.stringify(cond.relativeHumidity)}
                            </p>
                            <p>
                              visibility:
                              {JSON.stringify(cond.visibility)}
                            </p>
                            <p>
                              wind:
                              {JSON.stringify(cond.wind)}
                            </p>
                            <p>
                              period:
                              {JSON.stringify(cond.period)}
                            </p>
                            <br></br>
                            <hr></hr>
                          </div>
                        );
                      }
                    )}
                  </div>
                )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
