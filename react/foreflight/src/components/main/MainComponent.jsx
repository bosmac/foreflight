import React from "react";

export class MainComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ""};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    window.open("/#/detail/"+this.state.value, "_self");
  }

  render() {
    return (
      <div className="input-group justify-content-center pt-5 mt-5">
          <input type="text" value={this.state.value} onChange={this.handleChange} />
        <div className="input-group-append">
          <button className="btn btn-outline-primary" type="button" onClick={this.handleSubmit}>
            Search
          </button>
        </div>
      </div>
    );
  }
}
