import React from "react";
import logo from "../../assets/logo.png";
import "./Styles.css";

export class NavComponent extends React.Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-md navbar-dark bg-dark mynav mb-4">
          <a className="navbar-brand" href="/">
            <img src={logo} width="50%"></img>
          </a>
        </nav>
      </div>
    );
  }
}
