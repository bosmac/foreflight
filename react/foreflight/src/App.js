import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import { MainPage, DetailPage } from "./pages";

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={MainPage} />
          <Route exact path="/home" component={MainPage} />
          <Route exact path="/detail/:id" component={DetailPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
