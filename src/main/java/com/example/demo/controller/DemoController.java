package com.example.demo.controller;

import com.example.demo.dto.Airport;
import com.example.demo.dto.Report;
import com.example.demo.dto.ResponseDto;
import com.example.demo.service.DemoService;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/foreflight/v1")
public class DemoController {
    private static final Logger logger = LogManager.getLogger(DemoController.class);
    private DemoService demoService;

    @Autowired
    public DemoController(DemoService demoService){
        this.demoService = demoService;
    }

    @GetMapping("/weather/{id}")
    public ResponseEntity<Report> getWeatherData(@PathVariable("id") String id) {
        try{
            logger.info("DemoController.getWeatherData() id: {}", id);
            Report jsonNode = demoService.getWeatherData(id);
            logger.info("DemoController.getWeatherData() jsonNode: {}", jsonNode);
            return new ResponseEntity<>(jsonNode, HttpStatus.OK);
        }catch (Exception ex){
            logger.error("DemoController.getWeatherData() id: {}", id, ex);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/airport/{id}")
    public ResponseEntity<Airport> getAirportData(@PathVariable("id") String id) {
        try{
            logger.info("DemoController.getAirportData() id: {}", id);
            Airport jsonNode = demoService.getAirportData(id);
            logger.info("DemoController.getAirportData() jsonNode: {}", jsonNode);
            return new ResponseEntity<>(jsonNode, HttpStatus.OK);
        }catch (Exception ex){
            logger.error("DemoController.getAirportData() id: {}", id, ex);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/airport-weather-data/{id}")
    public ResponseEntity<ResponseDto> getAirportAndWeatherData(@PathVariable("id") String id) {
        try{
            logger.info("DemoController.getAirportAndWeatherData() id: {}", id);
            Airport airport = demoService.getAirportData(id);
            Report report = demoService.getWeatherData(id);
            ResponseDto responseDto = new ResponseDto();
            responseDto.setAirtportData(airport);
            responseDto.setWeatherData(report);
            logger.info("DemoController.getAirportAndWeatherData() responseDto: {}", responseDto);
            return new ResponseEntity<>(responseDto, HttpStatus.OK);
        }catch (Exception ex){
            logger.error("DemoController.getAirportAndWeatherData() id: {}", id, ex);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
