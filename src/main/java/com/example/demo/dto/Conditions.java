package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Conditions {
    private String tempC;
    private String relativeHumidity;
    private Visibility visibility;
    private Wind wind;
    private Period period;
}
