package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Airport {
    private String icao;
    private String city;
    private String state;
    private String country;
    private String name;
    private String latitude;
    private String longitude;
    private List<Runway> runways;
}
