package com.example.demo.service;

import com.example.demo.dto.Airport;
import com.example.demo.dto.Report;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Service
public class DemoService {

    private static final Logger logger = LogManager.getLogger(DemoService.class);
    private final String BASE_ENDPOINT = "https://qa.foreflight.com";
    private RestTemplate restTemplate;

    @Autowired
    public DemoService(){
        this.restTemplate = new RestTemplate();
    }

    public DemoService(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    public Report getWeatherData(String airportId) throws JsonProcessingException {
        if (airportId == null){
            throw new IllegalArgumentException("airportId cannot be null");
        }
        logger.info("DemoService.getWeatherData() airportId: {}", airportId);
        String url = BASE_ENDPOINT+"/weather/report/"+airportId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("ff-coding-exercise", "1");
        headers.set("Authorization", getAuth());
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        if(response == null || response.getBody() == null){
            throw new IllegalStateException("API response cannot be null");
        }
        logger.info("DemoService.getWeatherData() api response: {}", response);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JsonNode jsonNode =  mapper.readTree(response.getBody());
        JsonNode jsonNodeReport = jsonNode.get("report");
        logger.info("DemoService.getWeatherData() jsonNode: {}", jsonNodeReport);
        Report report = mapper.convertValue(jsonNodeReport, Report.class);
        logger.info("DemoService.getWeatherData() report: {}", report);
        return report;
    }

    public Airport getAirportData(String airportId) throws JsonProcessingException {
        if (airportId == null){
            throw new IllegalArgumentException("airportId cannot be null");
        }
        logger.info("DemoService.getAirportData() airportId: {}", airportId);
        String url = BASE_ENDPOINT+"/airports/"+airportId;
        HttpHeaders headers = new HttpHeaders();
        headers.set("ff-coding-exercise", "1");
        headers.set("Authorization", getAuth());
        HttpEntity entity = new HttpEntity(headers);
        ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
        if(response == null || response.getBody() == null){
            throw new IllegalStateException("API response cannot be null");
        }
        logger.info("DemoService.getAirportData() api response: {}", response);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        JsonNode jsonNode = mapper.readTree(response.getBody());
        Airport airport = mapper.convertValue(jsonNode, Airport.class);
        logger.info("DemoService.getAirportData() jsonNode: {}", airport);
        return airport;
    }

    private String getAuth(){
        String auth = "ff-interview:@-*KzU.*dtP9dkoE7PryL2ojY!uDV.6JJGC9";
        byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(StandardCharsets.US_ASCII) );
        String authHeader = "Basic " + new String( encodedAuth );
        return authHeader;
    }
}
