package com.example.demo;

import com.example.demo.controller.DemoController;
import com.example.demo.dto.Airport;
import com.example.demo.dto.Report;
import com.example.demo.dto.ResponseDto;
import com.example.demo.service.DemoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public class DemoControllerTest {
    private DemoService demoService = Mockito.mock(DemoService.class);
    private DemoController demoController = new DemoController(demoService);

    @Test
    public void getWeatherData() throws JsonProcessingException {
        Mockito.when(demoService.getWeatherData(Mockito.anyString())).thenReturn(new Report());
        ResponseEntity<Report> reoot = demoController.getWeatherData("KAUS");
        Assertions.assertNotNull(reoot);
    }

    @Test
    public void getAirportData() throws JsonProcessingException {
        Mockito.when(demoService.getAirportData(Mockito.anyString())).thenReturn(new Airport());
        ResponseEntity<Airport> reoot = demoController.getAirportData("KAUS");
        Assertions.assertNotNull(reoot);
    }

    @Test
    public void getAirportAndWeatherData() throws JsonProcessingException {
        Mockito.when(demoService.getWeatherData(Mockito.anyString())).thenReturn(new Report());
        Mockito.when(demoService.getAirportData(Mockito.anyString())).thenReturn(new Airport());
        ResponseEntity<ResponseDto> reoot = demoController.getAirportAndWeatherData("KAUS");
        Assertions.assertNotNull(reoot);
    }

}
